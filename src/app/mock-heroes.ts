import { Hero } from './hero';

export const HEROES: Hero[] = [
    {id: 11, name: 'Marisa'},
    {id: 12, name: 'Batista'},
    {id: 13, name: 'Marista'},
    {id: 14, name: 'Barter'},
    {id: 15, name: 'Salfetka'},
    {id: 16, name: 'Tefal'},
    {id: 17, name: 'Grigori Skovoroda'},
    {id: 18, name: 'Madam'},
];
